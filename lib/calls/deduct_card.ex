defmodule DeductCard do
  Module.register_attribute(__MODULE__, :topics, accumulate: true)

  @topics "deduct card"
  @topic "deduct card2"

  @on_load :subscribe

  def subscribe do
    IO.puts("Start")
    Stack.register(__MODULE__, @topics)
  end
end
