defmodule DeductCredit do

  def deduct(_topic, variables) do
    if Map.get(variables, "amount") > 100 do
      {:error, "network nicht ereicht"}
      {:ok, %{creditSufficient: false}}
    else
      # amount abbuchen
      {:ok, %{creditSufficient: true}}
    end
  end

  def deduct_card(_topic, _variables) do
    {:ok, %{}}
    {:bpmn_error, "Error_Deduct_Card", %{}}
  end

  def add_credit(_topic, variables) do
    amount = Map.get(variables, "amount")
    IO.inspect(amount, label: "Drauf buchen")
    {:ok, %{}}
  end

  def payment_completed(_topic, _variables) do
    Api.send_message("receive payment")
    {:ok, %{}}
  end
end
