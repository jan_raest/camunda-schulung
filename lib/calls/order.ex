defmodule Order do

  def generate_business_key(_topic, _vars) do
    {:ok, %{business_key: to_string(:rand.uniform(100000000))}}
  end

  def start_payment(_topic, vars) do
    Api.send_message("zahlung eingegangen")
    {:ok, %{amount: Map.get(vars, "amount")}}
  end
end
