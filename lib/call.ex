defmodule Call do
  use Task

  def start_link(_state) do
    IO.puts("Starte Call:")
    Task.start_link(&poll/0)
  end


  def poll() do
    receive do
    after
      1_000 ->
        IO.puts("Starte Abfrage an Camunda:")
        Api.call_camunda()
        poll()
    end
  end
end
