defmodule Api do
  use Tesla

  plug(Tesla.Middleware.BaseUrl, "http://localhost:8080/engine-rest")
  plug(Tesla.Middleware.JSON)

  def register do
    [
      %{topic: "deduct credit", lock_duration: 1000, fun: &DeductCredit.deduct/2},
      %{topic: "deduct card", lock_duration: 1000, fun: &DeductCredit.deduct_card/2},
      %{topic: "add credit", lock_duration: 1000, fun: &DeductCredit.add_credit/2},
      %{topic: "payment completed", lock_duration: 1000, fun: &DeductCredit.payment_completed/2},

      %{topic: "generate business key", lock_duration: 1000, fun: &Order.generate_business_key/2},
      %{topic: "start payment", lock_duration: 1000, fun: &Order.start_payment/2}
    ]
  end

  defmodule Request do
    @derive Jason.Encoder

    defstruct [
      :workerId,
      :maxTasks,
      :topics
    ]
  end

  def call_camunda do
    topics = Enum.map(register(), fn x -> %{topicName: x.topic, lockDuration: x.lock_duration} end)
    {:ok, response} = call_external(topics)
    list = response.body

    if list != [] do
      element = List.first(list)
      IO.puts("For ID:" <> Map.get(element, "id"))

      fun = get_fun(register(), Map.get(element, "topicName"))
      IO.inspect(fun, label: "Function: ")

      fun.(Map.get(element, "topicName"), format_variables(Map.get(element, "variables")))
      |> case do
        {:ok, var} -> handle_succes(Map.get(element, "id"), define_variables(var))
        {:error, msg} -> handle_error(Map.get(element, "id"), msg)
        {:bpmn_error, error_code, var} -> handle_bpmn_error(Map.get(element, "id"), error_code, define_variables(var))
      end
    end
  end

  def get_fun(register, topic) do
    Enum.find(register, fn x -> x.topic == topic end)
    |> Map.get(:fun)
  end

  def format_variables(var) do
    Enum.reduce var, %{}, fn {k, v}, acc -> Map.merge(%{k => Map.get(v, "value")}, acc) end    # Enum.map(var, fn {k, v} -> %{k => Map.get(v, "value")} end)
  end

  def define_variables(vars) do
    Enum.reduce vars, %{}, fn {k, v}, acc -> Map.merge(%{k => %{value: v, type: get_json_type(v)}}, acc) end
  end

  def handle_succes(id, variables) do
    IO.puts("handle_succes")
    req = %{
      workerId: "Worker:1",
      variables: variables
    }

    IO.inspect(req)

    post("/external-task/#{id}/complete", req)
  end

  def handle_error(id, msg) do
    IO.puts("handle_error")
    req = %{
      workerId: "Worker:1",
      errorMessage: msg,
      # errorDetails: msg,
      retries: 0
    }

    post("/external-task/#{id}/failure", req)
  end

  def handle_bpmn_error(id, code, variables) do
    IO.puts("handle_bpmn_error")
    req = %{
      workerId: "Worker:1",
      errorCode: code,
      # errorCode: code,
      # errorMessage: message,
      variables: variables
    }

    post("/external-task/#{id}/bpmnError", req)
    |> IO.inspect()
  end

  def call_external(topics) do
    req = %Api.Request{
      workerId: "Worker:1",
      maxTasks: 1,
      topics: topics
    }

    post("external-task/fetchAndLock", req)
  end

  def send_message() do
    var = %{amount: 139.90, test: true}
    req = %{
      "messageName" => "Zahlung",
      "processVariables" => define_variables(var)
    }

    post("/message", req)
  end

  def send_message(msg, vars \\ %{}) do
    req = %{
      "messageName" => msg,
      "processVariables" => vars
    }

    post("/message", req)
  end


  def get_json_type(var) do
    case var do
      x when is_bitstring(x) -> "String"
      x when is_binary(x) -> "String"
      x when is_boolean(x) -> "Boolean"
      x when is_integer(x) -> "Integer"
      x when is_float(x) -> "Double"
      _ -> "String"
    end
  end

end
